
# Conteúdo Programático ANDROID

## 1. Introdução ao Desenvolvimento Mobile
- O que é um aplicativo mobile?
    - camada de apresentação
    - consumir serviço web
    - banco de dados
    - algo que der um feedback rápido
- Metodologia offline first

## 2. Linguagem de Programação Kotlin

https://kotlinlang.org/docs/tutorials/kotlin-for-py/introduction.html

- Variávies e constantes
    - var
        - inicialização tardia
    - val
        - inicialização preguiçosa
    - const
- Condicionais
    - if/else
    - when
    - comparações
- Operadores
    - operator
    - plus (+)
    - minus (-)
    - times (*)
    - div (/)
    - rem (%)
    - rangeTo (..)
    - not (!)
    - in (contains)
    - plusAssign(+=)
    - minusAssign(-=)
    - timesAssign(*=)
    - divAssign(/=)
    - remAssign(%=)
   
- Loops
    - repeat
    - for
    - while
    - continue e break
- Null-safety
    - operador de chamada segura
    - operador elvis
    - operador acertivo não-nulo
- Exceptions
- Funções
    - funçoes literais (  val print = { println("oi eu sou uma funcao literal") })
    - funções locais
    - varargs
    - parametros nomeados
    - infix
    - Unit
    - run, let, with
    - apply, also
    - takeIf, takeUnless
    - use
- Lambdas
    - expressões
- Funções de alta ordem
    - função passada como parametro
    - função que retorna um função
- Classes
    - init
    - abstract class
    - genéricos
    - data class
    - sealed class
    - enum class
    - herança
    - interface
    - composição
    - singleton
    - companion objects
    - getters e setters
    - casting e teste de tipos
- Extensions
- Coroutines ???
- Boas práticas

## 3. Git

## 4. Apresentação do Ambiente de Desenvolvimento
- Android Studio
- Simulador
- Postman

## 5. REST

## 4. Android

## 5. Arquitetura

## 6. Projeto de Conclusão
