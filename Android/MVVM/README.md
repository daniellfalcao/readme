# MVVM 

O MVVM(Model View ViewModel) é um padrão de software arquitetural, isto é, uma solução genérica e reutilizável para resolver um problema 
que acontece numa arquitetura de um software dado um contexto. 

O objetivo do MVVM é prover uma separação de responsabilidades, entre a view e sua lógica, contudo, isto agrega benefícios como aumento da testabilidade da aplicação. 

Separação de responsabilidades? Aumentar a testabilidade do app? Sim, a criação de testes para o Android não é algo simples. A falta de organização e a mistura entre responsabilidades pode gerar acoplamento entre o seu código e o código do Sistema Operacional (SO), ou seja, seu teste pode ficar impossibilitado de executar na JVM pois pode existir alguma dependência que necessite de classes do SO para ser executada.

## Estrutura
Já foi explicado alguns dos benefícios de sua utilização em aplicações Android, portanto vamos abordar um pouco sobre como o padrão se organizaria em uma aplicação.

<p align="center">
  <img src="https://cdn-images-1.medium.com/max/800/1*a0S8WVx21BQeqFziKqxCGQ.png">
</p>

### View
A view no MVVM é a entidade responsável por definir a estrutura, layout e aparência do que será exibido na tela. Dentro do nosso contexto, as Views são nossas Activities, Fragments e elementos visuais criados para serem disponibilizados na tela. A view simplesmente recebe os dados de algum lugar e os exibe. Coisas como animações, carregamento de imagens, permissões, fazem parte dessa camada.

### Model 
O model no MVVM,  encapsula a lógica de negócios e os dados. O Modelo nada mais é do que o Modelo de domínio de uma aplicação, ou seja, as classes de negócio que serão utilizadas em uma determinada aplicação. O Modelo também contém os papéis e também a validação dos dados de acordo com o negócio, cuja aplicação em questão visa atender. 

### ViewModel 
O view model no MVVM, age como intermediário entre a View e o Model, é o responsável por manusear o Model para ser utilizado pela View. 

## Vantagens
Como já foi comentado antes o objetivo do MVVM é prover uma separação de responsabilidades, com isso a primeira vantagem do MVVM atende ao primeiro princípio do **S.O.L.I.D.** (Princípios da Programação Orientada a Objetos) que é o **SSRP** (Single Responsibility Principle) que diz: Uma classe deve ter um, e somente um, motivo para mudar.

* O view model é desacoplado da view. Em outras palavras, o view model jamais sabe quem o está utilizando. Dessa forma, você pode 
reutilizar e testar facilmente sua classe view model. Essa é a razão principal para utilizar o MVVM.
* A lógica de exibição dos dados fica restrita às views, em outras palavras, a view é que sabe como os dados devem ser exibidos.
* O código fica mais limpo e organizado.
* Facilita a leitura e a manutenção do código.
* Testabilidade do seu projeto.

## Visão Global do Android ViewModel
Antes de começar a implementar o view model precisamos saber de alguns detalhes sobre a classe [ViewModel](https://developer.android.com/reference/android/arch/lifecycle/ViewModel) do Android.

* A classe ViewModel é designada para armazenar e manusear dados relacionados à interface do usuário de uma maneira consciente do ciclo de vida.
* A classe ViewModel permite que os dados sobrevivam a mudanças de configurações como por exemplo a rotação da tela.

O framework do Android manuseia o ciclo de vida dos controladores de interface (UI controller), como activities e fragments. O framework 
em algum momento pode decidir destruir ou recriar toda uma UI controller em resposta a uma ação do usuário ou evento do dispositivo que 
estão completamente fora de seu controle.

Se o framework destrói ou recria uma UI controller, qualquer dado armazenado dentro da UI controller é perdido. Por exemplo, se o seu app tem uma lista de usuários armazenado em sua activity, quando a activity é recriada por causa de alguma mudança de configuração, a nova activity tem que recarregar a lista de usuários. Para dados simples pode se usar o método [onSaveInstanceState( )](https://developer.android.com/reference/android/app/Activity#onSaveInstanceState(android.os.Bundle)) e restaurar os dados do bundle no [onCreate( )](https://developer.android.com/reference/android/app/Activity#onCreate(android.os.Bundle)), mas essa abordagem só é adequada para pequenas quantidades de dados.

Outro problema que pode ocorrer na UI controller acontece quando é preciso fazer alguma chamada assíncrona que pode levar algum tempo para retornar. A UI controller precisa gerenciar essas chamadas e garantir que o sistema as limpe depois de ser destruído para evitar possíveis memory leaks (vazamentos de memória). Esse gerenciamento requer muita manutenção e, no caso em que o objeto é recriado por causa de alguma mudança de configuração, é um desperdício de recursos, pois o objeto pode precisar refazer as chamadas que já foram feitas.

## Antes de Continuar...
Alguns conceitos necessários para o que vem a seguir:

* [LifecycleOwner](https://developer.android.com/reference/android/arch/lifecycle/LifecycleOwner): LifecycleOwner é uma classe que possui o ciclo de vida do Android. Esses eventos podem ser usados por componentes personalizados para lidar com alterações do ciclo de vida sem implementar nenhum código dentro da activity ou fragment.

* [Lifecycle](https://developer.android.com/reference/android/arch/lifecycle/Lifecycle): Lifecycle define um objeto que possui um ciclo de vida do Android. As classes Fragment e FragmentActivity implementam a interface LifecycleOwner, que possui o método [getLifecycle( )](https://developer.android.com/reference/android/arch/lifecycle/LifecycleOwner#getLifecycle()) para acessar o ciclo de vida. Também é possível implementar o LifecycleOwner em suas próprias classes.

* [Observer](https://developer.android.com/reference/android/arch/lifecycle/Observer): Um callback de chamada simples que pode receber do LiveData.

* [LiveData](https://developer.android.com/reference/android/arch/lifecycle/LiveData): LiveData é uma classe de suporte de dados que pode ser observada dentro de um determinado ciclo de vida. Isso significa que um Observer pode ser adicionado com um LifecycleOwner, e este observador será notificado sobre modificações dos dados somente se o LifecycleOwner que foi associado a ele estiver no estado ativo. O LifecycleOwner é considerado ativo, se o seu estado for STARTED ou RESUMED. O Observer será automaticamente removido se o ciclo de vida do seu LifecycleOwner passar para o estado DESTROYED.

* [ViewModelProvider](https://developer.android.com/reference/android/arch/lifecycle/ViewModelProvider): ViewModelPovider é uma classe de utilitário que fornece um ViewModel para um determinado escopo.

## Implementando um ViewModel
O Android Architecture Components disponibiliza a classe ViewModel. Os objetos ViewModel são retidos automaticamente durante as alterações de configuração, de modo que os dados retidos estejam imediatamente disponíveis para a próxima activity ou fragment. Por exemplo, se você precisa exibir uma lista de usuários no seu aplicativo, atribua a responsabilidade de adquirir e manter a lista de usuários a um ViewModel, em vez de atribuir a uma activity ou fragment, conforme mostrado no seguinte código de exemplo:

```
class MyViewModel : ViewModel() {

    private lateinit var users: MutableLiveData<List<User>>

    fun getUsers(): LiveData<List<User>> {
        if (!::users.isInitialized) {
            users = MutableLiveData()
            loadUsers()
        }
        return users
    }

    private fun loadUsers() {
        // Do an asynchronous operation to fetch users.
    }
}
```

Você pode acessar a lista de usuários da sua activity da seguinte forma:

```
class MyActivity : AppCompatActivity() {

    val model: MyViewModel by lazy { ViewModelProviders.of(this).get(MyViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        // Create a ViewModel the first time the system calls an activity's onCreate() method.
        // Re-created activities receive the same MyViewModel instance created by the first activity.

        model.getUsers().observe(this, Observer<List<User>>{ users ->
            // update UI
        })
    }
}
```

Se a activity for recriada, ela irá receber a mesma instância do MyViewModel que foi criada pela primeira vez. Quando o owner da activity é finalizado, o framework irá chamar método [onCleared( )](https://developer.android.com/reference/android/arch/lifecycle/ViewModel#onCleared()) para que possa limpar os recursos, como por exemplo, remover algum callback do viewmodel.

## CUIDADO
Um view model nunca deve referenciar uma view, lifecycle, ou qualquer classe que tenha alguma referencia a algum contexto da activity

Os objetos ViewModel são projetados para sobreviver a instanciações específicas de views ou LifecycleOwners. Objetos ViewModel podem conter LifecycleObservers, como objetos LiveData. No entanto, os objetos ViewModel nunca devem observar alterações nos observáveis que reconhecem o ciclo de vida, como os objetos LiveData. 

## Ciclo de Vida do ViewModel
Os objetos ViewModel têm o escopo definido para o Lifecycle passado ao ViewModelProvider na hora de sua criação. O view model permanece na memória até o escopo do Lifecycle desaparecer permanentemente. No caso de uma activity acontece quando ela é finalizada e no caso de um fragment quando ele é detached.

A figura abaixo ilustra os vários estados do ciclo de vida de uma activity, a ilustração também mostra a vida útil do view model ao lado do ciclo de vida da activity associada. Os mesmos estados básicos também se aplicam ao clico de vida de um fragment.

<p align="center">
  <img src="https://developer.android.com/images/topic/libraries/architecture/viewmodel-lifecycle.png">
</p>

## Compartilhamento de Dados Entre Fragments
Algumas vezes acontece de que dois ou mais fragments em uma activity precisem se comunicar uns com os outros. Imagine o caso comum de de fragments de detalhes, no qual você tem um fragment que usuário seleciona um item de uma lista e outro fragment que exibe o conteúdo do item selecionado. Este caso nunca é trivial pois ambos os fragments precisam definir alguma interface de comunicação, e a activity deve unir os dois. Além disso, ambos os fragments devem manipular o cenário em que o outro fragment ainda não foi criado ou não está visível.

Este ponto problemático pode ser resolvido usando ViewModel. Esses fragments podem compartilhar um ViewModel usando seu escopo de activity para lidar com essa comunicação, conforme mostrado no seguinte código.

```
class SharedViewModel : ViewModel() {
    val selected = MutableLiveData<Item>()

    fun select(item: Item) {
        selected.value = item
    }
}

class MasterFragment : Fragment() {

    private lateinit var itemSelector: Selector

    private lateinit var model: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        itemSelector.setOnClickListener { item ->
            // Update the UI
        }
    }
}

class DetailFragment : Fragment() {

    private lateinit var model: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = activity?.run {
            ViewModelProviders.of(this).get(SharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
        model.selected.observe(this, Observer<Item> { item ->
            // Update the UI
        })
    }
}
```

Note que ambos os fragments usam [getActivity( )](https://developer.android.com/reference/android/app/Fragment#getActivity()) quando vão usar o ViewModelProvider. Como resultado disso, os dois fragments recebem a mesma instância do SharedViewModel, cujo escopo está definido para a activity.

Essa abordagem oferece os seguintes benefícios:

* A activity não precisa fazer nada, ou saber alguma coisa sobre a comunicação dos dois fragments.
* Os fragments não precisam se conhecer para que funcionem. Se algum fragment desaparecer o outro contínua a funcionar normalmente.
* Cada fragment tem o seu próprio ciclo de vida, e não é afetado pelo ciclo de vida do outro. Se algum fragment for recriado, a interface do usuário contínua a funcionar sem problemas.

## Alguns Links
[devmedia](https://www.devmedia.com.br/entendendo-o-pattern-model-view-viewmodel-mvvm/18411) - Entendendo o Pattern Model View VIewMOdel MVVM

[medium](https://medium.com/brq-techblog/android-mvvm-e-databinding-fd4595e271c9) - Android - MVVM e Databinding

[medium](https://medium.com/@soutoss/arquiteturas-em-android-mvvm-kotlin-retrofit-parte-1-2ac77c8a26) - Arquiteturas em Android: MVVM + Kotlin + Retrofit Parte 1

[medium](https://medium.com/thiago-aragao/solid-princ%C3%ADpios-da-programa%C3%A7%C3%A3o-orientada-a-objetos-ba7e31d8fb25) - SOLID - Princípios da Programação Orientada a Objetos
