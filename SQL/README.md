# SQLite

</br>

### COMANDOS BÁSICOS DO TERMINAL DO SQLITE

</br>


- .schema nome_da_tabela >  mostra o esquema do banco
- .headers on >  mostra os headers das tabelas

</br>

### TIPOS DE DADOS

</br>

- text > texto
- varchar(tamanho) > texto
- integer > número inteiro
- number> número com casa decimal
- date > data
- boolean> booleano

</br>

### OPERAÇÕES EM TABELAS

</br>

- Criar uma tabela
```SQL
CREATE TABLE nome_da_tabela (nome_do_campo tipo_do_campo, ...) ;
```

- Deletar uma tabela
```SQL
DROP TABLE nome_da_tabela;
```

- Renomear uma tabela
```SQL
ALTER TABLE nome_da_tabela RENAME TO novo_nome_da_tabela;
```

- Inserir campo em uma tabela
```SQL
ALTER TABLE nome_da_tabela ADD nome_do_campo tipo_do_campo;
```

</br>

### OPERAÇÕES EM VIEW

</br>

- Cria uma VIEW
```SQL
CREATE VIEW nome_da_view AS algum_select
```

- Deleta uma VIEW
```SQL
DROP VIEW nome_da_view
```

</br>

### INSERÇÃO DE DADOS

</br>

```SQL
INSERT INTO nome_da_tabela (nome_do_campo, ...) VALUES (valor_do_campo, ...);
```

</br>

### SELEÇÃO DE DADOS

</br>

 - Seleciona todos os campos (*) da tabela nome_da_tabela
```SQL
SELECT * FROM nome_da_tabela;
```

- Seleciona somente os nome_do_campo da tabela nome_da_tabela
```SQL
SELECT nome_do_campo, nome_do_campo FROM nome_da_tabela;
```

- Seleciona todos os campos (*) da tabela nome_da_tabela onde o valor armazenado em nome_do_campo seja igual a valor_do_campo
```SQL
SELECT * FROM nome_da_tabela WHERE nome_do_campo=valor_do_campo
```

- Seleciona todos os campos (*) da tabela nome_da_tabela onde o valor armazenado em nome_do_campo seja igual a valor_do_campo, o IN vai funcionar como um OR para cada valor 
```SQL
SELECT * FROM nome_da_tabela WHERE nome_do_campo IN (valor_do_campo, ...)
```

- Seleciona todos os campos (*) da tabela nome_da_tabela onde onde o valor armazenado em nome_do_campo comece com valor_da_busca
- "%" indica que o resto não interessa, pode ser usado no começo ou fim.
```SQL
SELECT * FROM nome_da_tabela WHERE nome_do_campo LIKE "valor_do_campo%"
```

- Seleciona todos os campos (*) da tabela nome_da_tabela e ordena de forma crescente (ASC) ou descrescente (DESC)
```SQL
SELECT * FROM nome_da_tabela ORDER BY nome_do_campo ASC, nome_do_campo DESC
```

- Seleciona todos os campos (*) de duas tabelas onde o WHERE é valido
- AS renomeia a tabela ou campo
```SQL
SELECT l.titulo, a.nome AS autor_nome FROM livro AS l, autor AS a WHERE a.id = l.autor_id;
```

- Seleciona alguns campos e cria novos campos modificados de uma tabela
```SQL
SELECT nome_do_campo, nome_do_campo_novo1 + 20, nome_do_campo_novo2 * 1.10 FROM nome_da_tabela;
```

- Seleciona um campo de forma distinta ignorando resultados repetidos
```SQL
SELECT DISTINCT nome_do_campo FROM nome_da_tabela;
```

- Limita a quantidade de dados retornados do SELECT
```SQL
SELECT * FROM nome_da_tabela LIMIT valor_do_limite;
```

- Limita a quantidade de dados retornados do SELECT mas aplica um offset nos dados retornados
```SQL
SELECT * FROM nome_da_tabela LIMIT valor_do_limite OFFSET valor_do_offset;
``` 

- Seleciona os dados de uma tabela se satisfazer a condicao de ambos os lados e junta os campos das duas tabelas no resultado
```SQL
SELECT * FROM nome_da_tabela1 INNER JOIN nome_da_tabela2 ON nome_da_tabela.campo_da_tabela = nome_da_tabela.campo_da_tabela
```

- Seleciona os dados de uma tabela mesmo se não satisfazer a condicao de ambos os lados e junta os campos das duas tabelas no resultado da tabela a esquerda
```SQL
SELECT * FROM nome_da_tabela1 LEFT OUTER JOIN nome_da_tabela2 ON nome_da_tabela.campo_da_tabela = nome_da_tabela.campo_da_tabela
```

</br>

### ATUALIZAÇÃO DE DADOS

</br>

- Atualiza todos os registros da tabela nome_da_tabela com o valor_do_campo
```SQL
UPDATE nome_da_tabela set nome_do_campo=valor_do_campo
```

- Atualiza somente o registro da tabela nome_da_tabela com o valor_do_campo que corresponde ao WHERE
```SQL
UPDATE nome_da_tabela set nome_do_campo=valor_do_campo WHERE nome_do_campo=valor_do_campo
```

</br>

### DELEÇÃO DE DADOS

</br>

- Deleta todos os registos da tabela nome_da_tabela
```SQL
DELETE FROM nome_da_tabela
```

- Deleta todos os registros da tabela nome_da_tabela onde nome_do_campo sejá igual ao valor_do_campo
```SQL
DELETE FROM nome_da_tabela WHERE nome_do_campo=valor_do_campo
``` 

- Deleta todos os registros da tabela nome_da_tabela onde nome_do_campo sejá igual ao valor_do_campo
```SQL
DELETE FROM nome_da_tabela WHERE nome_do_campo=valor_do_campo AND nome_do_campo=valor_do_campo
``` 

- Deleta todos os registros da tabela nome_da_tabela onde nome_do_campo sejá igual ao valor_do_campo
```SQL
DELETE FROM nome_da_tabela WHERE nome_do_campo=valor_do_campo OR nome_do_campo=valor_do_campo
``` 

</br>

### FUNÇÕES DE AGREGAÇÃO

</br>

- Faz a contagem de quantos registros existem na tabela
```SQL
SELECT COUNT(*) FROM nome_da_tabela;
```

- Calcula a média dos valores de uma tabela
```SQL
SELECT AVG(nome_do_campo) FROM nome_da_tabela;
```

- Acha o valor máximo para um determinado campo de uma tabela
```SQL
SELECT MAX(nome_do_campo) FROM nome_da_tabela;
```

- Acha o valor mínimo para um determinado campo de uma tabela
```SQL
SELECT MIN(nome_do_campo) FROM nome_da_tabela;
```

- Agrupa e conta quantos valores existem de um SELECT
```SQL
SELECT nome_do_campo, COUNT(*) FROM nome_da_tabela GROUP BY nome_do_campo_agrupado;
```

- Agrupa e conta quantos valores existem de um SELECT mas limitado a clausula HAVING
```SQL
SELECT nome_do_campo, COUNT(*) FROM nome_da_tabela GROUP BY nome_do_campo_agrupado HAVING COUNT(*) < valor;
```

</br>

### FUNÇÕES

</br>

- (IFFNULL) - seleciona um campo da tabela, caso ele sejá nulo retorna o que foi passado como parâmetro
```SQL
select nome_do_campo1, IFNULL(nome_do_campo1, "o campo está null") FROM nome_da_tabela;
```

- (LENGHT) - retorna o tamanho da palavra
```SQL
SELECT nome_do_campo, LENGTH(nome_do_campo) FROM nome_da_tabela;
```

- (LOWER & UPPER) - deixa tudo minusculo ou maiusculo
```SQL
SELECT UPPER(nome_do_campo) FROM nome_da_tabela;
```

- (SUBSTR(x,y,z)) - faz um substring, pega o texto a partir da posicao y e pega os z primeiros caracteres a partir de y.
```SQL
SELECT SUBSTR("String", 3, 3);
``` 

- (RANDOM) - Gera um número randomico
```SQL
SELECT RANDOM();
```

- (REPLACE(x,y,z)) - troca todas as ocorrencias de y em x por z
```SQL
SELECT REPLACE("CURSO DE SQL", " ", "---");
```

- (ROUND(x, y)) - Arredonda o valor de x para y casas decimais
```SQL
SELECT ROUND(nome_do_campo, 1) FROM nome_da_tabela;
```

</br>

### TRANSACTION

</br>

```SQL
BEGIN TRANSACTION
    -- Realiza varias acoes
COMMIT;
```

</br>

### TRIGGERS

</br>

```SQL
CREATE TRIGGER nome_da_trigger AFTER INSERT ON nome_da_tabela
BEGIN
    -- Faz a ação sempre depois que for adicionado alguma coisa na nome_da_tabela
END;
```