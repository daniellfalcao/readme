# Clean Architecture

Clean Architecture combina um grupo de práticas para produzir sistemas que tem as seguintes caracteristicas:

- <b>Independente de framework:</b> A arquitetura não deve depender da existência de alguma biblioteca de software cheio de recursos. Isso permite que você use esse framework como ferramenta, em vez de força-lo a colocar seu sistema em restrições limitadas. 
- <b>Independente de UI:</b> A UI pode mudar facilmente  sem mudar o resto do sistema.
- <b>Independente de database:</b> Você pode mudar do Realm para o Room ou algo parecido. Suas regras de negócio não estão ligadas ao banco de dados.
- <b>Testável:</b> Suas regras de negócio podem ser testáveis sem a UI, database, web service, ou qualquer elemento externo

## A regra da dependência

<p align="center">
  <img width="500" height="500" src="arquivos/the-clean-architecture.png">
</p>